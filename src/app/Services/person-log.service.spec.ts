import { TestBed } from '@angular/core/testing';

import { PersonLogService } from './person-log.service';

describe('PersonLogService', () => {
  let service: PersonLogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonLogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
