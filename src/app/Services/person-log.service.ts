import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { personDetails } from '../model/personDetails';

const ITEMS_KEY = 'my-items'

@Injectable({
  providedIn: 'root'
})
export class PersonLogService {

  constructor(public storage: Storage) { }

  addItem(item: personDetails): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((items: personDetails[])=>{
      if(items){
        items.push(item);
        return this.storage.set(ITEMS_KEY,items)
      }else{
        return this.storage.set(ITEMS_KEY,[item])
      }
    })
  }

  getItems(): Promise<personDetails[]>{
    return this.storage.get(ITEMS_KEY);
  }
}
