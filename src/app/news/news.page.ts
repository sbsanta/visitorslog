import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  newsUpdates:any;
  latestNews:any
  constructor(private http: HTTP) { }

  ngOnInit() {
    this.newsApi();
  }

  newsApi(){
    this.http.get('http://newsapi.org/v2/everything?q=bitcoin&from=2020-06-03&sortBy=publishedAt&apiKey=402468b5af224df2bd1789a327f3d943',
     {}, {})
  .then(data => {

    console.log(data.status);
    console.log(data.data); // data received by server
    this.newsUpdates = JSON.parse(data.data)
    console.log(data.headers);
    console.log("Newsupdates", this.newsUpdates.articles);
    this.latestNews = this.newsUpdates.articles
    console.log("latestnews", this.latestNews);
    

  })
  .catch(error => {

    console.log(error.status);
    console.log(error.error); // error message as string
    console.log(error.headers);

  })

  }

}
