import { Component, ViewChild, OnInit } from '@angular/core';
import { personDetails } from '../model/personDetails';
// import { List } from '@ionic/angular';
import { PersonLogService } from '../Services/person-log.service';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

items: personDetails[] = []
newItem: personDetails = <personDetails>{};
submitted: boolean = false;
// @ViewChild('mylist')mylist : List;
myDate = new Date()
typeOfVisit:any;
  constructor(public personLog: PersonLogService, public navCtrl: NavController) {
    // this.loadItems();
  }

  radioGroupChange(event) {
    this.typeOfVisit = event.detail.value;
    }


  addItem(form: NgForm){
    this.submitted = true
    if(form.valid){
      console.log("formdata", form.value)
      console.log("newItem", this.newItem);
      this.personLog.addItem(this.newItem).then(item=>{
      this.newItem = <personDetails>{}
      console.log("added")
      // this.loadItems();
      this.navCtrl.pop();
    })
  }else{
    console.log("please fill all the fields");
  }
  }

  ngOnInit(){
    this.newItem.date = this.newItem.date = this.myDate.toISOString().substring(0, 10)
    this.newItem.type_of_visit = this.typeOfVisit
  }



  // loadItems(){
  //   this.personLog.getItems().then(items =>{
  //     this.items = items;
  //   })
  // }

 

}

