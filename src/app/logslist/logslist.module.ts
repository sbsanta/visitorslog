import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogslistPageRoutingModule } from './logslist-routing.module';

import { LogslistPage } from './logslist.page';
import { PersonLogService } from '../Services/person-log.service';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogslistPageRoutingModule,
    IonicStorageModule.forRoot(),
  
  ],
  declarations: [LogslistPage],
  providers: [Storage,PersonLogService]
})
export class LogslistPageModule {}
