import { Component, OnInit } from '@angular/core';
import { PersonLogService } from '../Services/person-log.service';
import { personDetails } from '../model/personDetails';

@Component({
  selector: 'app-logslist',
  templateUrl: './logslist.page.html',
  styleUrls: ['./logslist.page.scss'],
})
export class LogslistPage implements OnInit {

  items: personDetails[] = []
   
  constructor(public personLog: PersonLogService) { 
    this.loadItems();
  }

  ngOnInit() {
    
  }

  

  ionViewWillEnter(){
    this.loadItems()
  }

  loadItems(){
    this.personLog.getItems().then(items =>{
      this.items = items;
    })
  }
}
