import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogslistPage } from './logslist.page';

const routes: Routes = [
  {
    path: '',
    component: LogslistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogslistPageRoutingModule {}
