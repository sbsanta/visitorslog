import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LogslistPage } from './logslist.page';

describe('LogslistPage', () => {
  let component: LogslistPage;
  let fixture: ComponentFixture<LogslistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogslistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LogslistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
